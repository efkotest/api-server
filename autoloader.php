<?php
$path = dirname(__FILE__);

require_once($path . '/classes/helpers/jwt_helper.php');

require_once($path . '/config/auth.php');
require_once($path . '/config/database.php');

require_once($path . '/classes/Utils.php');
require_once($path . '/classes/User.php');
require_once($path . '/classes/DataBase.php');
require_once($path . '/classes/Route.php');