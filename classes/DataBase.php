<?php
require_once(dirname(__FILE__).'/Route.php');
require_once(dirname(__FILE__).'/../config/database.php');

class DataBase extends mysqli {

  public function __construct() {
    parent::__construct(
      DATABASE_HOST,
      DATABASE_USERNAME,
      DATABASE_PASSWORD,
      DATABASE_BASENAME,
      DATABASE_PORT);
    if ($this->connect_errno !== 0) {
      return [
        'error' => [
          'message' => 'Data Base error',
          'debug' => 'Failed to initialize connection to MySQL server ' .
          $this->connect_errno .
          $this->connect_error
        ]
      ];
    }
    if ($this->set_charset(DATABASE_CHARSET) !== TRUE) {
      return [
        'error' => [
          'message' => 'Data Base error',
          'debug' => 'Failed to set connection charset ' .
          $this->connect_errno .
          $this->connect_error
        ]
      ];
    }
    return true;
  }

  public function getRow($query) {
    $result = $this->query($query);
    $row = $result->fetch_assoc();

    $result->close();
    return $row;
  }

  public function getRows($query) {
    $result = $this->query($query);
    $rows = $result->fetch_all(MYSQLI_ASSOC);

    $result->close();
    return $rows;
  }

  public function closeConnection() {
    $this->close();
  }

}