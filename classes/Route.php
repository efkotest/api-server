<?php

class Route extends DataBase {

  private $module = null;
  private $route = '';
  private $method = '';
  private $debug = false;
  public $user = null;

  public function __construct($route, $method, $debug=false) {
    $this->route = $route;
    $this->method = strtolower($method);
    $this->debug = $debug;

    $this->checkRoute();

    // after success check route, now open a mysql connection
    $dataBaseError = parent::__construct()['error'] ?? null;
    if ($dataBaseError) {
      $this->renderError(
        'Route call error: ' . $this->route . '('.$dataBaseError['message'].')',
        $dataBaseError['debug']
      );
    }

    // mysql connection success created, we should check user authorization
    if ($route !== '/authorization') {
      $this->user = new User($this);
      $authorizationError = $this->user->checkAuthorization()['error'] ?? null;
      if ($authorizationError) {
        $this->renderError(
          $authorizationError['message'],
          $authorizationError['debug']
        );
      }
    }
  }

  public function render($params=[]) {
    $result = call_user_func([$this->module, $this->method], $params);
    if (isset($result['error'])) {
      return $this->renderError($result['message'], $result['debug']);
    }
    $this->closeConnection();
    die(json_encode(
      $result,
      JSON_UNESCAPED_UNICODE
    ));
  }

  public function renderError($message='Server error', $debug=[]) {
    $this->closeConnection();
    $response = [
      'error' => -1,
      'message' => $message
    ];
    if ($this->debug === TRUE) {
      $response['debug'] = $debug;
    }
    die(json_encode($response));
  }

  private function checkRoute() {
    $path = './routes'.$this->route.'.php';

    if (file_exists($path) === FALSE) {
      $this->renderError(
        'Route call error: ' . $this->route.' isn\'t exists',
        '['.$path.'] '.$this->method.' '
      );
    }
    require_once($path);
    if (class_exists($routeClassName) === FALSE) {
      $this->renderError(
        'Route call error: ' . $this->route.' server error',
        '['.$path.'] '.$this->method.' '.$this->route .
        ' set route class name to same like `$routeClassName`'
      );
    }
    if (method_exists($routeClassName, $this->method) === FALSE) {
      $this->renderError(
        'Route call error: ' . $this->route.' server error',
        '['.$path.'] '.$this->method.' '.$this->route .
        $routeClassName . ' isn\'t have a needed method: ' . $this->method
      );
    }
    $this->module = new $routeClassName($this);
  }

}