<?php

class User extends Utils {

  private $router = null;
  private $jwtPayload = null;
  private $permissions = null;

  /**
   * User constructor.
   * @param $router Route
   */
  public function __construct($router) {
    $this->router = $router;
  }

  public function getId() {
    return $this->getPayload()->userid;
  }

  public function isAbbleTo($permission) {
    return in_array($permission, $this->getPermissions());
  }

  public function checkAuthorization() {
    if (empty($this->getPayload())) {
      return [
        'error' => [
          'error' => 1,
          'message' => 'Ошибка проверки авторизации.',
          'debug' => 'Authorization header empty or jwt corrupted'
        ]
      ];
    }

    if ($this->getPayload()->userid >= 1) {
      return true;
    }

    return [
      'error' => [
        'error' => 3,
        'message' => 'Ошибка авторизации'
      ]
    ];
  }

  private function getJwt() {
    return $_SERVER['HTTP_AUTHORIZATION'] ?? $_COOKIE[AUTH_JWT_COOKIENAME] ?? null;
  }

  private function getPayload() {
    if (!$this->jwtPayload) {
      $this->jwtPayload = parent::getJwtPayload($this->getJwt());
    }
    return $this->jwtPayload;
  }

  private function getPermissions() {
    if (!$this->permissions) {
      $userId = $this->router->escape_string($this->getPayload()->userid);
      $user = $this->router->getRow("SELECT permissions FROM users WHERE id = {$userId}");
      $this->permissions = json_decode($user['permissions']);
    }
    return $this->permissions;
  }
}