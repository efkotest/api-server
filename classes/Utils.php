<?php

class Utils {

  private $jwt = null;
  private $jwtPayload = null;

  public function getJwtPayload($jwt) {
    $this->jwt = $jwt;
    if (empty($this->jwt)) {
      return false;
    }

    try {
      $this->jwtPayload = JWT::decode($this->jwt, AUTH_JWT_SECRET);
    }
    catch (Error $error) {
      return false;
    }

    return $this->jwtPayload;
  }

}