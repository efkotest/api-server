<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Methods: *');
require_once('./autoloader.php');

$route = $_GET['route'] ?? null;
$method = $_SERVER['REQUEST_METHOD'];

if ($method === 'OPTIONS') {
  die(200);
}

$Route = new Route($route, $method, $debug=true);

switch ($method) {
  case 'GET':
    $Route->render();
    break;
  case 'POST';
    $Route->render($_POST);
    break;
  case 'PUT';
    echo 'PUT REQUEST DETECTED';
    break;
  case 'PATCH';
    parse_str(file_get_contents('php://input'), $_PATCH);
    $Route->render($_PATCH);
    break;
  case 'DELETE';
    echo 'DELETE REQUEST DETECTED';
    break;
}