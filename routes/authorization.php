<?php

$routeClassName = 'Authorization';

class Authorization {

  private $router = null;

  /**
   * Authorization constructor.
   * @param $router Route
   */
  public function __construct($router) {
    $this->router = $router;
  }

  public function post($params) {
    $userLogin = $this->router->escape_string($params['login']);
    $userPaswordHash = $this->router->escape_string(hash('sha256', $params['password']));

    $userData = $this->router->getRow("SELECT * FROM users WHERE email = '{$userLogin}'");
    if (empty($userData)) {
      return [
        'error' => 1,
        'message' => 'Сотрудник не найден!'
      ];
    }
    if (hash_equals($userData['password'], $userPaswordHash) === TRUE) {
      try {
        $jwt = JWT::encode([
          'firstName' => $userData['firstName'],
          'lastName' => $userData['lastName'],
          'userid' => $userData['id'],
          'permissions' => json_decode($userData['permissions'], true),
        ], AUTH_JWT_SECRET, AUTH_JWT_ALGORITHM);
      }
      catch (Error $error) {
        return [
          'error' => 2,
          'message' => 'Произошла техническая ошибка. Сообщите администратору!',
          'debug' => $error
        ];
      }
      setcookie('eftt_token', $jwt);
      return [
        'message' => 'Вы успешно авторизованы!',
        'token' => $jwt
      ];
    }

    return [
      'error' => 2,
      'message' => 'Email или пароль указан неверно!'
    ];
  }

}