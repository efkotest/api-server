<?php

$routeClassName = 'Vacation';

class Vacation {

  private $router = null;

  /**
   * VacationsList constructor.
   * @param $router Route
   */
  public function __construct($router) {
    $this->router = $router;
  }

  public function patch($params) {
    if ($this->router->user->isAbbleTo('vacationsManage') !== TRUE) {
      return [
        'error' => 5,
        'message' => 'У вас нет прав подтверждать отпуск!'
      ];
    }
    // format date
    $vacationId = $this->router->escape_string($params['id']);
    $status = $this->router->escape_string($params['status']);

    $vacationData = $this->router->getRow("SELECT * FROM  vacations WHERE id = {$vacationId}");
    if (empty($vacationData)) {
      return [
        'error' => 4,
        'message' => 'Заявка на отпуск не найдена!'
      ];
    }

    if ((int) $vacationData['status'] === 2) {
      return [
        'error' => '3',
        'message' => 'Отпуск одобрен другим руководителем!'
      ];
    }
    else if ((int) $vacationData['status'] === 1) {
      return [
        'error' => '2',
        'message' => 'Отпуск отклонен другим руководителем!'
      ];
    }

    if (!$this->router->query("UPDATE vacations SET status = {$status} WHERE id = {$vacationId}")) {
      return [
        'error' => 1,
        'message' => 'Произошла ошибка. Пожалуйста, сообщите администратору.',
        'debug' => $this->router->error . '(' . $this->router->errno . ') '
      ];
    }

    switch ($status) {
      case 2:
        $messageText = "Заявка на отпуск #{$vacationId} одобрена!";
        break;
      case 1:
        $messageText = "Заявка на отпуск #{$vacationId} отклонена!";
        break;
      default:
        return [
          'error' => -1,
          'message' => 'Произошла ошибка. Пожалуйста, сообщите администратору.',
          'debug' => $this->router->error . '(' . $this->router->errno . ') Status: ' . $status
        ];
    }

    return [
      'message' => $messageText
    ];
  }
}
