<?php

$routeClassName = 'VacationStatus';

class VacationStatus {

  private $router = null;

  /**
   * VacationStatus constructor.
   * @param $router Route
   */
  public function __construct($router) {
    $this->router = $router;
  }

  public function get() {
    $userId = $this->router->escape_string($this->router->user->getId());
    $vacation = $this->router->getRow(
      "SELECT vac.id, vac.status FROM vacations vac RIGHT JOIN users u ON vac.userid = u.id WHERE u.id = {$userId}"
    );

    return [
      'id' => (int) $vacation['id'] ?? 0,
      'status' => (int) $vacation['status'] ?? 0
    ];
  }
}