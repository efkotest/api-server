<?php

$routeClassName = 'VacationsList';

class VacationsList {

  private $router = null;

  /**
   * VacationsList constructor.
   * @param $router Route
   */
  public function __construct($router) {
    $this->router = $router;
  }

  public function get() {
    $items = $this->router->getRows(
      'SELECT vac.id, vac.userid, DATE_FORMAT(vac.startDate, "%d.%m.%Y") AS startDate, DATE_FORMAT(vac.endDate, "%d.%m.%Y") AS endDate, vac.status, u.firstName, u.lastName FROM vacations vac, users u WHERE u.id = vac.userid ORDER BY status DESC'
    );
    return [
      'items' => $items
    ];
  }

  public function post($params) {
    if ($this->router->user->isAbbleTo('vacationPlanning') !== TRUE) {
      return [
        'error' => 3,
        'message' => 'У вас нет прав планировать отпуск!'
      ];
    }
    // format date
    $params['startDate'] = date('Y-m-d', strtotime($params['startDate']));
    $params['endDate'] = date('Y-m-d', strtotime($params['endDate']));

    $userId = $this->router->escape_string($this->router->user->getId());
    $vacStartDate = $this->router->escape_string($params['startDate']);
    $vacEndDate = $this->router->escape_string($params['endDate']);

    $userVacation = $this->router->getRow(
      "SELECT * FROM vacations WHERE userid = {$userId} LIMIT 1");
    if (empty($userVacation)) {
      $resultSuccess = $this->router->query(
        "INSERT INTO vacations (id, userid, startDate, endDate, status) VALUES(DEFAULT, {$userId}, '{$vacStartDate}', '{$vacEndDate}', DEFAULT)"
      );
      if ($resultSuccess === TRUE) {
        return [
          'message' => 'Ваш отпуск отправлен на рассмотрение руководителю.'
        ];
      }
      return [
        'error' => -1,
        'message' => 'Произошла техническия ошибка. Пожалуйста, сообщите администратору.',
        'debug' => $this->router->error . '(' . $this->router->errno . ')'
      ];
    }
    if ($userVacation['status'] !== 0) {
      return [
        'error' => 2,
        'message' => 'Вы не можете изменить дату подтвержденного или отклоненного отпуска!'
      ];
    }
    return [
      'message' => 'Ваш отпуск все еще находится на рассмотрении, но вы можете скорректировать даты.'
    ];
  }

  public function patch($params) {
    if ($this->router->user->isAbbleTo('vacationPlanning') !== TRUE) {
      return [
        'error' => 4,
        'message' => 'У вас нет прав планировать отпуск!'
      ];
    }
    // format date
    $params['startDate'] = date('Y-m-d', strtotime($params['startDate']));
    $params['endDate'] = date('Y-m-d', strtotime($params['endDate']));

    $userId = $this->router->escape_string($this->router->user->getId());
    $vacStartDate = $this->router->escape_string($params['startDate']);
    $vacEndDate = $this->router->escape_string($params['endDate']);

    $userVacation = $this->router->getRow(
      "SELECT * FROM vacations WHERE userid = {$userId} LIMIT 1");
    if (empty($userVacation)) {
      return [
        'error' => 3,
        'message' => 'Произошла ошибка. Пожалуйста, сообщите администратору.',
        'debug' => $this->router->error . '(' . $this->router->errno . ') ' . $userId
      ];
    }
    if ($userVacation['status'] === 2) {
      return [
        'error' => 2,
        'message' => 'Вы не можете изменить дату подтвержденного или отклоненного отпуска!'
      ];
    }
    if (!$this->router->query("UPDATE vacations SET startDate = '{$vacStartDate}', endDate = '{$vacEndDate}', status = 0 WHERE id = {$userVacation['id']}")) {
      return [
        'error' => 1,
        'message' => 'Произошла ошибка. Пожалуйста, сообщите администратору.',
        'debug' => $this->router->error . '(' . $this->router->errno . ') '
      ];
    }
    return [
      'message' => 'Даты отпуска изменены.'
    ];
  }

}